package com.web.finalproject.services;

import com.web.finalproject.entities.Incomes;
import com.web.finalproject.forms.IncomesForm;

import java.time.LocalDate;
import java.util.List;

public interface IncomesService {
    Incomes getIncomes(Long id);

    void deleteIncomes(Long id);

    void updateIncomes(IncomesForm form, Long id);

    List<Incomes> getIncomesByUser(Long userId);

    void addIncomesToUser(IncomesForm form, Long userId);

    List<Incomes> getIncomesReportByUserAndByIncomeTypeAndPeriod(
            Long userId, Long incomeTypeId, LocalDate dateFrom, LocalDate dateTo);

    List<Incomes> getIncomesReportByUserAndPeriod(
            Long userId, LocalDate dateFrom, LocalDate dateTo);

    Double sumOfIncomesByPeriod(Long userId, LocalDate dateFrom, LocalDate dateTo);

    Double sumOfIncomesByIncomeTypeAndPeriod(
            Long userId, Long incomeTypeId, LocalDate dateFrom, LocalDate dateTo);

    Double incomesOfCurrentMonth(Long userId);
}
