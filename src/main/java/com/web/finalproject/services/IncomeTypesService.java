package com.web.finalproject.services;

import com.web.finalproject.entities.IncomeType;
import com.web.finalproject.forms.IncomeTypeForm;

import java.util.List;

public interface IncomeTypesService {
    List<IncomeType> getAll();

    IncomeType getIncomeTypeById(Long id);

    void addIncomeType(IncomeTypeForm form);

    void deleteIncomeType(Long id);

    void updateIncomeType(IncomeTypeForm form, Long id);
}
