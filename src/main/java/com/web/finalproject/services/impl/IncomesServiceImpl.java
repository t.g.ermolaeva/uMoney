package com.web.finalproject.services.impl;

import com.web.finalproject.entities.Incomes;
import com.web.finalproject.entities.User;
import com.web.finalproject.forms.IncomesForm;
import com.web.finalproject.repositories.IncomesRepository;
import com.web.finalproject.repositories.UsersRepository;
import com.web.finalproject.services.IncomesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Service
public class IncomesServiceImpl implements IncomesService {

    private final IncomesRepository incomesRepository;
    private final UsersRepository usersRepository;

    @Override
    public Incomes getIncomes(Long id) {
        return incomesRepository.getById(id);
    }

    @Override
    public void deleteIncomes(Long id) {
        incomesRepository.deleteById(id);
    }

    @Override
    public void updateIncomes(IncomesForm form, Long id) {
        Incomes incomes = incomesRepository.getById(id);
        incomes.setDate(form.getDate());
        incomes.setIncomeType(form.getIncomeType());
        incomes.setAmount(form.getAmount());
        incomes.setNote(form.getNote());

        incomesRepository.save(incomes);
    }

    @Override
    public List<Incomes> getIncomesByUser(Long userId) {
        return incomesRepository.findFirst10ByUserIdOrderByDateDesc(userId);
    }

    @Override
    public void addIncomesToUser(IncomesForm form, Long userId) {
        User user = usersRepository.getById(userId);

        Incomes incomes = Incomes.builder()
                .date(form.getDate())
                .incomeType(form.getIncomeType())
                .amount(form.getAmount())
                .note(form.getNote())
                .build();

        incomes.setUser(user);
        incomesRepository.save(incomes);
    }

    @Override
    public List<Incomes> getIncomesReportByUserAndByIncomeTypeAndPeriod(
            Long userId, Long incomeTypeId, LocalDate dateFrom, LocalDate dateTo) {
        return incomesRepository
                .findAllByUserIdAndIncomeTypeIdAndDateBetweenOrderByDateDesc(userId, incomeTypeId, dateFrom, dateTo);
    }

    @Override
    public List<Incomes> getIncomesReportByUserAndPeriod(
            Long userId, LocalDate dateFrom, LocalDate dateTo) {
        return incomesRepository
                .findAllByUserIdAndDateBetweenOrderByDateDesc(userId, dateFrom, dateTo);
    }

    @Override
    public Double incomesOfCurrentMonth(Long userId) {
        LocalDate dateTo = LocalDate.now();
        LocalDate dateFrom = dateTo.withDayOfMonth(1);
        return incomesRepository
                .findAllByUserIdAndDateBetween(userId, dateFrom, dateTo)
                .stream()
                .mapToDouble(Incomes::getAmount)
                .sum();
    }

    @Override
    public Double sumOfIncomesByPeriod(Long userId, LocalDate dateFrom, LocalDate dateTo) {
        return incomesRepository
                .findAllByUserIdAndDateBetween(userId, dateFrom, dateTo)
                .stream()
                .mapToDouble(Incomes::getAmount)
                .sum();
    }

    @Override
    public Double sumOfIncomesByIncomeTypeAndPeriod(Long userId, Long incomeTypeId, LocalDate dateFrom, LocalDate dateTo) {
        return incomesRepository
                .findAllByUserIdAndIncomeTypeIdAndDateBetween(userId, incomeTypeId, dateFrom, dateTo)
                .stream()
                .mapToDouble(Incomes::getAmount)
                .sum();
    }
}
