package com.web.finalproject.services.impl;

import com.web.finalproject.entities.User;
import com.web.finalproject.exceptions.UserNotFoundException;
import com.web.finalproject.forms.UserForm;
import com.web.finalproject.repositories.UsersRepository;
import com.web.finalproject.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService, UserDetailsService {

    private final UsersRepository usersRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public void deleteUser(Long id) {
        usersRepository.deleteById(id);
    }

    @Override
    public User getUserById(Long id) {
        return usersRepository.findById(id).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public void updateUser(Long id, UserForm form) {
        User user = usersRepository.getById(id);

        user.setFirstName(form.getFirstName());
        user.setLastName(form.getLastName());

        usersRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return usersRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }
}
