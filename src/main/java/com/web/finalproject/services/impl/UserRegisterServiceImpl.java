package com.web.finalproject.services.impl;

import com.web.finalproject.entities.User;
import com.web.finalproject.forms.UserRegisterForm;
import com.web.finalproject.repositories.UsersRepository;
import com.web.finalproject.services.UserRegisterService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserRegisterServiceImpl implements UserRegisterService {

    @Autowired
    private final PasswordEncoder passwordEncoder;

    @Autowired
    private final UsersRepository usersRepository;

    @Override
    public void registerUser(UserRegisterForm form) {
        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .role(User.Role.USER)
                .build();

        usersRepository.save(user);
    }
}
