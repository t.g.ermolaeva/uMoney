package com.web.finalproject.services.impl;

import com.web.finalproject.entities.IncomeType;
import com.web.finalproject.forms.IncomeTypeForm;
import com.web.finalproject.repositories.IncomeTypesRepository;
import com.web.finalproject.services.IncomeTypesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IncomeTypesServiceImpl implements IncomeTypesService {

    private final IncomeTypesRepository incomeTypesRepository;

    @Autowired
    public IncomeTypesServiceImpl(IncomeTypesRepository incomeTypesRepository) {
        this.incomeTypesRepository = incomeTypesRepository;
    }

    @Override
    public List<IncomeType> getAll() {
        return incomeTypesRepository.findAll();
    }

    @Override
    public IncomeType getIncomeTypeById(Long id) {
        return incomeTypesRepository.getById(id);
    }

    @Override
    public void addIncomeType(IncomeTypeForm form) {
        IncomeType incomeType = IncomeType.builder()
                .name(form.getName())
                .build();

        incomeTypesRepository.save(incomeType);
    }

    @Override
    public void deleteIncomeType(Long id) {
        incomeTypesRepository.deleteById(id);
    }

    @Override
    public void updateIncomeType(IncomeTypeForm form, Long id) {
        IncomeType incomeType = incomeTypesRepository.getById(id);
        incomeType.setName(form.getName());

        incomeTypesRepository.save(incomeType);
    }
}
