package com.web.finalproject.controller;

import com.web.finalproject.forms.IncomeTypeForm;
import com.web.finalproject.services.IncomeTypesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/incomeTypes")
public class IncomeTypesController {

    private final IncomeTypesService incomeTypesService;

    @Autowired
    public IncomeTypesController(IncomeTypesService incomeTypesService) {
        this.incomeTypesService = incomeTypesService;
    }

    @GetMapping
    public String getIncomeTypesPage(Model model) {
        model.addAttribute("incomeTypes", incomeTypesService.getAll());
        return "/income-type/income-types";
    }

    @PostMapping("/add")
    public String addIncomeType(@Valid IncomeTypeForm form) {
        incomeTypesService.addIncomeType(form);
        return "redirect:/incomeTypes";
    }

    @PostMapping("/delete")
    public String deleteIncomeType(@RequestParam("incomeTypeId") Long id) {
        incomeTypesService.deleteIncomeType(id);
        return "redirect:/incomeTypes";
    }

    @GetMapping("/incomeTypePage")
    public String getIncomePage(Model model, @RequestParam("incomeTypeId") Long id) {
        model.addAttribute("incomeType", incomeTypesService.getIncomeTypeById(id));
        return "/income-type/income-type-update-form";
    }

    @PostMapping("/incomeTypePage/update")
    public String updateIncomeType(@Valid IncomeTypeForm form, @RequestParam("incomeTypeId") Long id) {
        incomeTypesService.updateIncomeType(form, id);
        return "redirect:/incomeTypes";
    }
}
