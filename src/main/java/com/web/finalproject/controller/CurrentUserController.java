package com.web.finalproject.controller;

import com.web.finalproject.entities.User;
import com.web.finalproject.forms.UserForm;
import com.web.finalproject.services.ExpensesService;
import com.web.finalproject.services.IncomesService;
import com.web.finalproject.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@Controller
@RequestMapping("/currentUser")
public class CurrentUserController {

    private final UsersService usersService;
    private final ExpensesService expensesService;
    private final IncomesService incomesService;

    @GetMapping
    public String getCurrentUserPage(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User currentUser = (User)authentication.getPrincipal();
        Long id = currentUser.getId();

        model.addAttribute("user", usersService.getUserById(id));
        model.addAttribute("balance",
                incomesService.incomesOfCurrentMonth(id) - expensesService.expensesOfCurrentMonth(id));
        model.addAttribute("sumOfExpenses", expensesService.expensesOfCurrentMonth(id));
        model.addAttribute("sumOfIncomes", incomesService.incomesOfCurrentMonth(id));
        return "current-user";
    }

    @PostMapping("/update")
    public String updateUser(@RequestParam("currentUserId") Long id, UserForm form) {
        usersService.updateUser(id, form);
        return "redirect:/currentUser";
    }
}
