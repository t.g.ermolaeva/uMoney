package com.web.finalproject.controller;

import com.web.finalproject.entities.User;
import com.web.finalproject.forms.IncomesForm;
import com.web.finalproject.services.IncomeTypesService;
import com.web.finalproject.services.IncomesService;
import com.web.finalproject.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@RequiredArgsConstructor
@Controller
@RequestMapping("/incomes")
public class IncomesController {

    private final IncomeTypesService incomeTypesService;
    private final IncomesService incomesService;
    private final UsersService usersService;

    @GetMapping
    public String getIncomesByUser(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User currentUser = (User)authentication.getPrincipal();
        Long userId = currentUser.getId();

        model.addAttribute("user", usersService.getUserById(userId));
        model.addAttribute("incomesList", incomesService.getIncomesByUser(userId));
        model.addAttribute("incomeTypes", incomeTypesService.getAll());
        return "/incomes/incomes-by-user";
    }

    @PostMapping("/add")
    public String addIncomeToUser(@Valid IncomesForm form,
                                  @RequestParam ("currentUserId") Long userId) {
        incomesService.addIncomesToUser(form, userId);
        return "redirect:/incomes";
    }

    @PostMapping("/delete")
    public String deleteIncomeToUser(@RequestParam("currentUserId") Long userId,
                                     @RequestParam("incomesId") Long incomesId) {
        incomesService.deleteIncomes(incomesId);
        return "redirect:/incomes";
    }

    @GetMapping("/incomesPage")
    public String getIncomesPage(Model model,
                                 @RequestParam("incomesId") Long incomesId) {
        model.addAttribute("incomes", incomesService.getIncomes(incomesId));
        model.addAttribute("incomeTypes", incomeTypesService.getAll());
        return "/incomes/incomes-by-user-update-form";
    }

    @PostMapping("/incomesPage/update")
    public String updateIncomesToUser(@Valid IncomesForm form,
                                      @RequestParam("incomesId") Long incomesId) {
        incomesService.updateIncomes(form, incomesId);
        return "redirect:/incomes";
    }

    @GetMapping("/byDate")
    public String allIncomesReportByPeriod(Model model,
                                           @RequestParam("currentUserId") Long userId,
                                           @RequestParam("dateFrom") LocalDate dateFrom,
                                           @RequestParam("dateTo") LocalDate dateTo) {
        model.addAttribute("user", usersService.getUserById(userId));
        model.addAttribute("sumOfIncomes",
                incomesService.sumOfIncomesByPeriod(userId, dateFrom, dateTo));
        model.addAttribute("incomeTypes", incomeTypesService.getAll());
        model.addAttribute("incomesList",
                incomesService.getIncomesReportByUserAndPeriod(userId, dateFrom, dateTo));
        return "/incomes/incomes-by-user";
    }

    @GetMapping("/byIncomeType")
    public String expenseReportByIncomeAndPeriod(Model model,
                                                  @RequestParam("currentUserId") Long userId,
                                                  @RequestParam("incomeId") Long incomeTypeId,
                                                  @RequestParam("dateFrom") LocalDate dateFrom,
                                                  @RequestParam("dateTo") LocalDate dateTo) {
        model.addAttribute("user", usersService.getUserById(userId));
        model.addAttribute("sumOfIncomes",
                incomesService.sumOfIncomesByIncomeTypeAndPeriod(userId, incomeTypeId, dateFrom, dateTo));
        model.addAttribute("incomeTypes", incomeTypesService.getAll());
        model.addAttribute("incomesList",
                incomesService.getIncomesReportByUserAndByIncomeTypeAndPeriod(userId, incomeTypeId, dateFrom, dateTo));
        return "/incomes/incomes-by-user";
    }
}
