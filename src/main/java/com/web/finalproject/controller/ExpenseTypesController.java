package com.web.finalproject.controller;

import com.web.finalproject.forms.ExpenseTypeForm;
import com.web.finalproject.services.ExpenseTypesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/expenseTypes")
public class ExpenseTypesController {

    private final ExpenseTypesService expenseTypesService;

    @Autowired
    public ExpenseTypesController(ExpenseTypesService expenseTypesService) {
        this.expenseTypesService = expenseTypesService;
    }

    @GetMapping
    public String getExpenseTypesPage(Model model) {
        model.addAttribute("expenseTypes", expenseTypesService.getAll());
        return "/expense-type/expense-types";
    }

    @PostMapping("/add")
    public String addExpenseType(@Valid ExpenseTypeForm form) {
        expenseTypesService.addExpenseType(form);
        return "redirect:/expenseTypes";
    }

    @PostMapping("/delete")
    public String deleteExpenseType(@RequestParam("expenseTypeId") Long id) {
        expenseTypesService.deleteExpenseType(id);
        return "redirect:/expenseTypes";
    }

    @GetMapping("/expenseTypePage")
    public String getExpensePage(Model model, @RequestParam("expenseTypeId") Long id) {
        model.addAttribute("expenseType", expenseTypesService.getExpenseTypeById(id));
        return "/expense-type/expense-type-update-form";
    }

    @PostMapping("expenseTypePage/update")
    public String updateExpenseType(@Valid ExpenseTypeForm form, @RequestParam("expenseTypeId") Long id) {
        expenseTypesService.updateExpenseType(form, id);
        return "redirect:/expenseTypes";
    }
}
