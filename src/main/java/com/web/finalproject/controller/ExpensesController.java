package com.web.finalproject.controller;

import com.web.finalproject.entities.User;
import com.web.finalproject.forms.ExpensesForm;
import com.web.finalproject.services.ExpenseTypesService;
import com.web.finalproject.services.ExpensesService;
import com.web.finalproject.services.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@RequiredArgsConstructor
@Controller
@RequestMapping("/expenses")
public class ExpensesController {

    private final ExpenseTypesService expenseTypesService;
    private final ExpensesService expensesService;
    private final UsersService usersService;

    @GetMapping
    public String getExpensesByUser(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User currentUser = (User)authentication.getPrincipal();
        Long userId = currentUser.getId();

        model.addAttribute("user", usersService.getUserById(userId));
        model.addAttribute("expensesList", expensesService.getExpensesByUser(userId));
        model.addAttribute("expenseTypes", expenseTypesService.getAll());
        return "/expenses/expenses-by-user";
    }

    @PostMapping("/add")
    public String addExpenseToUser(@Valid ExpensesForm form, @RequestParam("currentUserId") Long userId) {
        expensesService.addExpensesToUser(form, userId);
        return "redirect:/expenses";
    }

    @PostMapping("/delete")
    public String deleteExpenseToUser(@RequestParam("currentUserId") Long userId,
                                      @RequestParam("expensesId") Long id) {
        expensesService.deleteExpenses(id);
        return "redirect:/expenses";
    }

    @GetMapping("/expensesPage")
    public String getExpensesPage(Model model,
                                  @RequestParam("expensesId") Long expensesId) {
        model.addAttribute("expenses", expensesService.getExpenses(expensesId));
        model.addAttribute("expenseTypes", expenseTypesService.getAll());
        return "/expenses/expenses-by-user-update-form";
    }

    @PostMapping("/expensesPage/update")
    public String updateExpensesToUser(@Valid ExpensesForm form,
                                       @RequestParam("expensesId") Long expensesId) {
        expensesService.updateExpenses(form, expensesId);
        return "redirect:/expenses";
    }

    @GetMapping("/byDate")
    public String allExpensesReportByPeriod(Model model,
                                            @RequestParam("currentUserId") Long userId,
                                            @RequestParam("dateFrom") LocalDate dateFrom,
                                            @RequestParam("dateTo") LocalDate dateTo) {
        model.addAttribute("user", usersService.getUserById(userId));
        model.addAttribute("sumOfExpenses",
                expensesService.sumOfExpenses(userId, dateFrom, dateTo));
        model.addAttribute("expenseTypes", expenseTypesService.getAll());
        model.addAttribute("expensesList",
                expensesService.getExpensesReportByUser(userId, dateFrom, dateTo));
        return "/expenses/expenses-by-user";
    }

    @GetMapping("/byExpenseType")
    public String expensesReportByExpenseAndPeriod(Model model,
                                                   @RequestParam("currentUserId") Long userId,
                                                   @RequestParam("expenseTypeId") Long expenseTypeId,
                                                   @RequestParam("dateFrom") LocalDate dateFrom,
                                                   @RequestParam("dateTo") LocalDate dateTo) {
        model.addAttribute("user", usersService.getUserById(userId));
        model.addAttribute("sumOfExpenses",
                expensesService.sumOfExpensesByExpenseType(userId, expenseTypeId, dateFrom, dateTo));
        model.addAttribute("expenseTypes", expenseTypesService.getAll());
        model.addAttribute("expensesList",
                expensesService.getExpensesReportByUserAndByExpenseType(userId, expenseTypeId, dateFrom, dateTo));
        return "/expenses/expenses-by-user";
    }
}
