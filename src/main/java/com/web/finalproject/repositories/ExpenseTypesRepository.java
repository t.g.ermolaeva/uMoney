package com.web.finalproject.repositories;

import com.web.finalproject.entities.ExpenseType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExpenseTypesRepository extends JpaRepository<ExpenseType, Long> {
}
