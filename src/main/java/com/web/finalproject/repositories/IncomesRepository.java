package com.web.finalproject.repositories;

import com.web.finalproject.entities.Incomes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface IncomesRepository extends JpaRepository<Incomes, Long> {
    List<Incomes> findFirst10ByUserIdOrderByDateDesc(Long userId);

    List<Incomes> findAllByUserIdAndIncomeTypeIdAndDateBetweenOrderByDateDesc(
            Long userId, Long incomeTypeId, LocalDate dateFrom, LocalDate dateTo);

    List<Incomes> findAllByUserIdAndIncomeTypeIdAndDateBetween(
            Long userId, Long incomeTypeId, LocalDate dateFrom, LocalDate dateTo);

    List<Incomes> findAllByUserIdAndDateBetweenOrderByDateDesc(
            Long userId, LocalDate dateFrom, LocalDate dateTo);

    List<Incomes> findAllByUserIdAndDateBetween(
            Long userId, LocalDate dateFrom, LocalDate dateTo);
}
