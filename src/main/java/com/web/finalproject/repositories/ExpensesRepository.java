package com.web.finalproject.repositories;

import com.web.finalproject.entities.Expenses;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ExpensesRepository extends JpaRepository<Expenses, Long> {
    List<Expenses> findFirst10ByUserIdOrderByDateDesc(Long userId);

    List<Expenses> findAllByUserIdAndExpenseTypeIdAndDateBetweenOrderByDateDesc(
            Long userId, Long expenseTypeId, LocalDate dateFrom, LocalDate dateTo);

    List<Expenses> findAllByUserIdAndExpenseTypeIdAndDateBetween(
            Long userId, Long expenseTypeId, LocalDate dateFrom, LocalDate dateTo);

    List<Expenses> findAllByUserIdAndDateBetweenOrderByDateDesc(
            Long userId, LocalDate dateFrom, LocalDate dateTo);

    List<Expenses> findAllByUserIdAndDateBetween(
            Long userId, LocalDate dateFrom, LocalDate dateTo);
}
