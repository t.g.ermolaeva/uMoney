package com.web.finalproject.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Data
public class UserForm {

    @NotEmpty
    @Length(min = 1, max = 20)
    private String firstName;

    @NotEmpty
    @Length(min = 1, max = 20)
    private String lastName;

}
